# Tennis Tournament System

Round Robin system including multiple tournaments, player ranks and player stats.

Developed using:

* [Electron](https://electronjs.org/)
* [Atom](https://atom.io/)
* [Nedb](https://github.com/louischatriot/nedb)
* [node.js](https://nodejs.org/en/)


---
## Installation
~~//Download the correct release build for you OS under "App/release-builds/..."~~

At the moment all releases are only accessable to be downloaded with the whole repository.

They are located within "App/release-builds/..." -> Mac, Windows, Linux.

Drag the specified folder onto your desktop or into you applications folder.

Mac:

* double click on the application to open.

Windows:

* locate the .exe file within the folder copied and then double click.

---
## Creating a Tournament
Selecting the "+" button in the top right of the tournament sidebar (left).

This will open up a new window that will require:

1. A tournament name
2. Player names (4+)
3. Confirmation of creation

After confirmation the window will close and the new tournament will be displayed to the user in the sidebar.

This tournament will be a default/blank tournament with no player statistics and displaying the first round.

Each player will have a rank given by their position when inputted into the tournament.

Ranks will be changed after each round has been played and scores inputted.

---
## Current matches and adding scores
Once a tournament has been selected the dashboard will display the Tournament title, ladder, current round and current matches.

The ladder will display the players in order of rank based on their Set Win % and Game Win %.

The current round will be displayed next to the player ladder title.

Its format is:

    'Current round (x)'

Matches to be played will be shown underneath the current round display.

At the end of each match scores will be added to the ladder, but the ladder will not update ranks till all matches have been completed.

Data that is required for each player when submitting the match is:

* Sets Won
* Sets Lost
* Games Won
* Games Lost

To submit a match all inputs for that match must be completed, if not an alert will display the error.

---
## Next round
Once all matches have been completed a 'next round' button will be displayed.

When clicked the ladder will be updated and players will be ordered by rank.

The current round number will be updated and the next matches to be played will displayed.

---
## Exporting data
The four buttons in the top right of the dashboard will provide options to:

1. Print all
2. Export all for email

> All buttons only export the currently viewed tournament.

**Print all**

Prints the screen using the:

    window.print()

This will open a "print screen" window to configure your print settings.

**Export for email**

Opens a new window with the correct formatting for an email.

Window contains tournaments name, ladder, matches and current round.

This would be for users to send out to players within the tournament.

There is a copy to clip-board button for ease of use.

    .select();
    document.execCommand("copy");
    
---
## Bug reports
Contact me with any enquiries.

Please keep explanations brief.

Email is not to be used for:

* Spam or unrelated emails

---
## Contact Me
email: jhill7177@gmail.com