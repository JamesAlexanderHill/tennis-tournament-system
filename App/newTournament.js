//nedb
var Datastore = require('nedb')
  , db = new Datastore({ filename: 'data.db', autoload: true });
//electron
const electron = require('electron');
const remote = require('electron').remote;
const {ipcRenderer} = electron;

//FUNCTIONS
function closeNewTournament(){//closes new tournament window
  //delete empty tournament
  var window = remote.getCurrentWindow();
  window.close();
}
function newPlayer(){//add a player input to new tournament page
  var inputList = document.getElementsByClassName('playerNameInput');
  var firstInput = inputList[0];
  var newInput = document.importNode(firstInput, true);
  document.getElementById("playerNameInputs").appendChild(newInput);
  //console.log(document.getElementById("playerNameInputs").lastChild);
  document.getElementById("playerNameInputs").lastChild.value = "";
}
function getData(){//tournament creation from data
  //check all inouts are filled
  var elements = document.getElementsByClassName('simpleInput');
  for(var i = 0; i < elements.length;i++){
    if(elements[i].value == ""){
      alert("fields empty");
      return false;
    }
  }
  //get data
  var title = elements[0].value;
  //append a player object for each player
  var playerArr = [];//add all players into this array
  //get playerNum
  var playerInputs = document.getElementsByClassName('playerNameInput');
  console.log(playerInputs.length);
  for(var i = 0; i < playerInputs.length;i++){
    var player = {name: playerInputs[i].value
                 ,rank: i+1
                 ,pos: i+1
                 ,played: 0
                 ,win: 0
                 ,loss: 0
                 ,setsWin: 0
                 ,setsLoss: 0
                 ,gameWin: 0
                 ,gameLoss: 0
                 ,winTemp: 0
                 ,lossTemp: 0
                 ,dataConfirmed: false};
    playerArr.push(player);
  }
  if(playerInputs.length%2 !=0){
    var byePlayer = {name: "bye"
                 ,pos: playerInputs.length+1
                 ,played: 0
                 ,win: 0
                 ,loss: 0
                 ,setsWin: 0
                 ,setsLoss: 0
                 ,gameWin: 0
                 ,gameLoss: 0
                 ,winTemp: 0
                 ,lossTemp: 0
                 ,setsWinTemp: 0
                 ,setsLossTemp: 0
                 ,gameWinTemp: 0
                 ,gameLossTemp: 0};
    playerArr.push(byePlayer);
  }

  /*[ {name: 'John'
               ,pos: 0
               ,played: 0
               ,win: 0
               ,loss: 0
               ,setsWin: 0
               ,setsLoss: 0
               ,gameWin: 0
               ,gameLoss: 0},
               {name: 'Peter'
               ,pos: 0
               ,played: 0
               ,win: 0
               ,loss: 0
               ,setsWin: 0
               ,setsLoss: 0
               ,gameWin: 0
               ,gameLoss: 0},
               {name: 'James'
               ,pos: 0
               ,played: 0
               ,win: 0
               ,loss: 0
               ,setsWin: 0
               ,setsLoss: 0
               ,gameWin: 0
               ,gameLoss: 0}]*/
  console.log(title);
  //add tournament to nedb
  var data = {tournamentTitle: title,
              currentRound: 1,
              players: playerArr};

  //send data to tournaments.js with ipc
  ipcRenderer.send('toMainWondow', data)
  //show success
  alert("Tournament Created");
  //close window
  var window = remote.getCurrentWindow();
  window.close();
}
function checkDocuments(){//prints all documents and errors
  db.find({}, function (err, docs) {
    console.log(docs);
    console.log("ERROR: " + err);
  });
}
function clearDocuments(){//clears documents from nedb
  db.remove({}, { multi: true }, function (err, numRemoved) {
  });
  loadTournamentsSidebar();
}
