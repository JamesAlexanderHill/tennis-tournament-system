const electron = require('electron');
const url = require('url');
const path = require('path');
//var Datastore = require('nedb'),db = new Datastore({ filename: 'data.db', autoload: true });

const {app, BrowserWindow, Menu, ipcMain} = electron;


//set env
process.env.NODE_ENV = 'development';
//process.env.NODE_ENV = 'production';

let mainWindow;
//listen for the app to be ready
app.on('ready', function(){
  //create new Window
  mainWindow = new BrowserWindow({});
  mainWindow.maximize();
  //load html into window
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, "mainWindow.html"),
    protocol:'file:',
    slashes: true
    //This creates this link => file://dirname/mainWindow.html
  }));
  mainWindow.on('focus', function(){
    //mainWindow.reload();
    //call a reload function to reload all data, clear old data but not reload the page (white flash)
    mainWindow.webContents.send('reloadPage', true)
  })
  //quit app when closed
  mainWindow.on('closed', function(){
    app.quit();
  })
  //build menu from mainMenuTemplate
  const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);
  //insert Menu
  Menu.setApplicationMenu(mainMenu);

});

//handle createAddWindow()
function createAddWindow(){
  //create new Window
  addWindow = new BrowserWindow({
    width: 300,
    height: 200,
    title: 'About SATC-App'
  });
  //load html into window
  addWindow.loadURL(url.format({
    pathname: path.join(__dirname, "addWindow.html"),
    protocol:'file:',
    slashes: true
    //This creates this link => file://dirname/mainWindow.html
  }));
  //garbage collection
  addWindow.on('close', function(){
    addWindow = null;
  })
}
//open newTournament window
ipcMain.on('newTournament', (event, arg) => {
  console.log(arg) // prints "ping"
  createNewTournamentWindow();
})
function createNewTournamentWindow(){
  //create new Window
  newTournamentWindow = new BrowserWindow({
    width: 300,
    height: 500,
    frame: true,
    title: 'New Tournament'
  });
  //load html into window
  newTournamentWindow.loadURL(url.format({
    pathname: path.join(__dirname, "newTournamentWindow.html"),
    protocol:'file:',
    slashes: true
    //This creates this link => file://dirname/newTournamentWindow.html
  }));
  //garbage collection
  newTournamentWindow.on('close', function(){
    newTournamentWindow = null;
  })
}

//handle createAboutWindow()
function createAboutWindow(){
  //create new Window
  aboutWindow = new BrowserWindow({
    width: 300,
    height: 200,
    title: 'About SATC-App'
  });
  //load html into window
  aboutWindow.loadURL(url.format({
    pathname: path.join(__dirname, "aboutWindow.html"),
    protocol:'file:',
    slashes: true
    //This creates this link => file://dirname/aboutWindow.html
  }));
  //garbage collection
  aboutWindow.on('close', function(){
    aboutWindow = null;
  })
}

//share Data
ipcMain.on('toMainWondow', (event, data) => {
    console.log(data) // prints "ping"
    mainWindow.webContents.send('dataMessage', data)
  })

//create menu template
const mainMenuTemplate = [
  {
    label:'File',
    submenu:[
      {
        label: 'About SATC-App',
        click(){
          createAboutWindow();
        }
      },
      {
        label: 'Quit',
        accelerator: process.platform == 'darwin' ? 'Command+Q' :
        'Ctrl+Q',
        click(){
          app.quit();
        }
      }
    ]
  }
];

//if mac add empty object to Menu
/*if(process.platform == 'darwin'){
  mainMenuTemplate.unshift({});
}*/
//add dev tools
if(process.env.NODE_ENV !== 'production'){
  mainMenuTemplate.push({
    label: 'Developer Tools',
    submenu: [
      {
        label: 'Toggle Dev Tools',
        accelerator: process.platform == 'darwin' ? 'Command+I' :
        'Ctrl+I',
        click(item, focusedWindow){
          focusedWindow.toggleDevTools();
        }
      },
      {
        role: 'reload'
      }
    ]
  })
}
