//electron
const electron = require('electron');
const remote = require('electron').remote;
const {ipcRenderer} = electron;
//nedb
const app = electron.remote.app;
var userData = app.getPath('userData')
var Datastore = require('nedb')
  , db = new Datastore({ filename: userData+'assets/db/data.db', autoload: true });
//round robbin setup
var rrSeq = [0, 2, -1, +2, -2, -1];
var displayedTournament = "";
//setup
window.onload = function(){//called on refresh page
  //reload tournament list
  loadTournamentsSidebar();
}
ipcRenderer.on('reloadPage', (event, bool) => {
    //console.log("Reload?: " + bool) // prints "pong"
    reloadData();
  })
function reloadDB(){
  db.loadDatabase(function (err) {    // Callback is optional
    if(err){
      console.log("Database Load err: " + err);
    }
    //console.log("Datastore reloaded");
  });
}
function reloadData(){
  // TODO: set displayedTournament to homepage
  //reload nedb
  reloadDB();
  //reload sidebar
  loadTournamentsSidebar();
  //reload laddder
  //reload round
  //reload currentMatches
  displayTournament(displayedTournament);
}
function loadTournamentsSidebar(){//updates the sidebar
  //clear current tournaments
  var tournamntParent = document.getElementById("tournaments")
  //console.log(tournamntParent.firstChild);
  while (tournamntParent.firstChild) {
    tournamntParent.removeChild(tournamntParent.firstChild);
    //console.log("removed");
  }
  //get tournament titles from nedb
  db.find({}, function (err, docs) {
    //console.log(docs.length);
    for(var i = 0; i < docs.length; i++){
      //console.log(docs[i].tournamentTitle);
      //add new li item
      var li = document.getElementById('tournamentLi');
      li.innerHTML = docs[i].tournamentTitle;
      var liElement = document.importNode(li, true);
      document.getElementById("tournaments").appendChild(liElement);
      //get last child of ul and set the id to docs.id
      //console.log(docs[i]._id);
      document.getElementById("tournaments").lastChild.setAttribute("id", docs[i]._id);
    }
  });
  //console.log("reloaded");
}
ipcRenderer.on('dataMessage', (event, data) => {
    console.log("data: " + data) // prints "pong"
    insertNewTournament(data);
  })
function insertNewTournament(tournament){
  db.insert(tournament, function (err, newDoc) {   // Callback is optional
    console.log(err);
    checkDocuments();
  });
}
//FUNCTIONS
function newTournament(){//initialises tournament
  ipcRenderer.send('newTournament', 'open new tournament window');
}
function checkDocuments(){//prints all documents and errors
  db.find({}, function (err, docs) {
    console.log(docs);
    if (err) {
      console.log("ERROR: " + err);
    }
  });
}
function clearDocuments(){//clears documents from nedb
  db.remove({}, { multi: true }, function (err, numRemoved) {
  });
  loadTournamentsSidebar();
}
function displayTournament(tournament){//displays contents of selected tournament
  displayedTournament = tournament
  console.log("Tournament ID: " + tournament.id);
  document.getElementById('playerLadderTable').innerHTML = '';
  document.getElementById('currentMatches').innerHTML = '';
  //remove all previous btns by class
  var nextRoundBtns = document.getElementsByClassName("nextRoundBtnClass");
  //console.log(nextRoundBtns);
  //console.log(nextRoundBtns.length);
  for(var o = 0; o < nextRoundBtns.length;o++){
    //console.log(nextRoundBtns[o]);
    nextRoundBtns[o].remove();
  }
  //clear selected
  var tournamentsUl = document.getElementsByClassName("tournamentInstance");
  for(var i = 0; i < tournamentsUl.length;i++){
    tournamentsUl[i].className = tournamentsUl[i].className.replace(/\bactive\b/g, "");
  }
  //add selected class
  tournament.classList.add("active");
  //get tournament from nedb
  // The same rules apply when you want to only find one document
  db.findOne({ _id: tournament.id }, function (err, doc) {
    console.log(doc);
    //change title to tournament title
    document.getElementById("tournamentNameVar").innerHTML = doc.tournamentTitle;
    //change playerLadder
    var tbody = document.getElementById("playerLadderTable");
    var count = 0;
    for(var i = 0; i < doc.players.length;i++){
      //make new row
      //console.log(tbody);
      if(doc.players[i].name != "bye"){
        var row = tbody.insertRow();
        for(var y = 0; y < 12;y++){
          row.insertCell(0)
        }
      }
      //order players by rank
      for(var x = 0; x < doc.players.length;x++){
        //console.log("Rank: " + (i+1), "Pos" + doc.players[x].pos);
        if(doc.players[x].pos == (i+1) && doc.players[i].name != "bye"){
          //console.log("Adddded");
          row.cells[0].innerHTML = i+1;
          //console.log(doc.players[0].name);
          row.cells[1].innerHTML = doc.players[x].name;
          row.cells[2].innerHTML = doc.players[x].played;
          row.cells[3].innerHTML = doc.players[x].win;
          row.cells[4].innerHTML = doc.players[x].loss;
          //Math.round((doc.players[i].setsWin/(doc.players[i].setsWin + doc.players[i].setsLoss) + 0.00001) * 100) / 100
          row.cells[5].innerHTML = Math.round((doc.players[x].win/doc.players[x].played + 0.00001) * 100);
          row.cells[6].innerHTML = doc.players[x].setsWin;
          row.cells[7].innerHTML = doc.players[x].setsLoss;
          row.cells[8].innerHTML = Math.round((doc.players[x].setsWin/(doc.players[x].setsWin + doc.players[x].setsLoss) + 0.00001) * 100);
          row.cells[9].innerHTML = doc.players[x].gameWin;
          row.cells[10].innerHTML = doc.players[x].gameLoss;
          row.cells[11].innerHTML = Math.round((doc.players[x].gameWin/(doc.players[x].gameWin + doc.players[x].gameLoss) + 0.00001) * 100);
        }
      }
      if(doc.currentRound > doc.players.length -1){
        var roundNum = document.getElementById("currentRoundNum").innerHTML = "Current Round: (FINISHED)";
      }else{
        //insert round number
        var roundNum = document.getElementById("currentRoundNum").innerHTML = "Current Round: ("+ doc.currentRound + ")";
        //insert li with every 2 Players
        var elementRound = document.getElementById('currentRoundElement');

        if(i%2==0 && doc.players[i].dataConfirmed == false){
          //player 1
          elementRound.children[0].children[0].children[0].innerHTML = doc.players[i].name;
          //player 2
          elementRound.children[0].children[0].children[1].innerHTML = doc.players[i+1].name;
          var liRoundElement = document.importNode(elementRound, true);
          document.getElementById("currentMatches").appendChild(liRoundElement);
          document.getElementById("currentMatches").lastChild.setAttribute("id", "");
          count = count + 1;
        }
      }
    }
    //console.log("COUNT: " + count);
    if(count == 0 && doc.currentRound <= doc.players.length -1){
      //console.log("TEST");
      //show next round btn
      //console.log("SHOW NEXT ROUND BTN");
      var nextRoundBtn = document.getElementById('nextRoundBtn');
      //console.log(nextRoundBtn);
      var btnNode = document.importNode(nextRoundBtn, true);
      btnNode.className = "nextRoundBtnClass";
      document.getElementById("currentRound").appendChild(btnNode);
      //document.getElementById("currentRound").lastChild.setAttribute("id", "");
    }else{

    }
  });

  //get data from nedb and display it on the page
}
function confirmData(btn){
  //console.log(btn.parentElement.parentElement);
  var match = btn.parentElement.parentElement;
  //console.log(match);
  //find player name
  db.findOne({ _id: displayedTournament.id }, function (err, doc) {
    var newPlayerArr = doc.players;
    var player1Pos = null;
    var player2Pos = null;
    for(var i = 0; i < newPlayerArr.length;i++){
      //find who player 1 and 2 are and make a reference
      if(newPlayerArr[i].name == match.children[0].children[0].innerHTML){
        player1Pos = i;
      }
      //console.log(match.children[0].children[1].innerHTML);
      if(newPlayerArr[i].name == match.children[0].children[1].innerHTML){
        player2Pos = i;
      }
    }
    if(player1Pos != null && player2Pos != null){
      //get data from inputs and add to newPlayerArray
      if(match.children[1].children[0].children[0].value && match.children[1].children[0].children[1].value && match.children[1].children[1].children[0].value && match.children[1].children[1].children[1].value){
        //console.log("Add data to player array");
        //add 1 to games played
        newPlayerArr[player1Pos].played += 1;
        newPlayerArr[player2Pos].played += 1;
        //check who won & lost the game
        //console.log(match.children[1].children[0].children[0].value + " > " + match.children[1].children[1].children[0].value);
        if(match.children[1].children[0].children[0].value > match.children[1].children[1].children[0].value){
          //console.log("player 1 wins");
          //add 1 to p1 wins
          newPlayerArr[player1Pos].win += 1;
          //add 1 to p2 loss
          newPlayerArr[player2Pos].loss += 1;
        }else{
          //console.log("player 2 wins");
          //add 1 to p2 wins
          newPlayerArr[player2Pos].win += 1;
          //add 1 to p1 loss
          newPlayerArr[player1Pos].loss += 1;
        }
        //set sets & games for each player
        newPlayerArr[player1Pos].setsWin += parseInt(match.children[1].children[0].children[0].value);
        //console.log(newPlayerArr[player1Pos].setsWin + " += " + match.children[1].children[0].children[0].value);
        newPlayerArr[player1Pos].setsLoss += parseInt(match.children[1].children[1].children[0].value);
        newPlayerArr[player1Pos].gameWin += parseInt(match.children[1].children[0].children[1].value);
        newPlayerArr[player1Pos].gameLoss += parseInt(match.children[1].children[1].children[1].value);

        newPlayerArr[player2Pos].setsWin += parseInt(match.children[1].children[1].children[0].value);
        newPlayerArr[player2Pos].setsLoss += parseInt(match.children[1].children[0].children[0].value);
        newPlayerArr[player2Pos].gameWin += parseInt(match.children[1].children[1].children[1].value);
        newPlayerArr[player2Pos].gameLoss += parseInt(match.children[1].children[0].children[1].value);
        //confirm data is submitted
        newPlayerArr[player1Pos].dataConfirmed = true;
        newPlayerArr[player2Pos].dataConfirmed = true;
      }else {
        alert("blank score input");
      }
      //update nedb with newPlayerArray
      //console.log("NewPlayerArr: " + newPlayerArr);
      db.update({ _id: displayedTournament.id }, { $set: { players: newPlayerArr } }, { multi: true }, function (errVar, numReplaced) {
        //console.log("Replaced Num: " + numReplaced);
        //console.log("ERROR: " + errVar);
        //console.log("DB Updated");
      });
      checkDocuments();
      //location.reload();
      reloadDB();
      //console.log("DB Reloaded");
      displayTournament(displayedTournament);
    }else{
      console.log("Player 1 or 2 can not be found in player list");
    }
  });
}

function nextRound(btn){
  var matchContainer = btn.parentElement;
  //console.log(matchContainer);
  db.findOne({ _id: displayedTournament.id }, function (err, doc) {
    tempPlayersArray = doc.players;
    nextRoundVar = (doc.currentRound+1);
    //console.log(doc.currentRound);
    for(var i = 0; i < tempPlayersArray.length; i++){
      if(tempPlayersArray[i].dataConfirmed == true){
        tempPlayersArray[i].dataConfirmed = false;
        //console.log(i + " changed");
      }
    }
    //step through players and change ranks
    //change all ranks to 0
    for(var i = 0; i < tempPlayersArray.length; i++){
      if(tempPlayersArray[i].pos != 0){
        tempPlayersArray[i].pos = 0;
      }
    }
    //sort by score
    var currRank = 1;
    for(var f = 0; f < tempPlayersArray.length; f++){
      for(var i = 0; i < tempPlayersArray.length; i++){
        if(tempPlayersArray[i].pos == 0){
          var max = tempPlayersArray[i];
          for(var x = 0; x < tempPlayersArray.length; x++){
            if(tempPlayersArray[x].pos == 0){
              if(compareScores(max,tempPlayersArray[x]) != 1){
                max = tempPlayersArray[x];
              }
            }
          }
          //console.log("MAX: " , max);
          max.pos = currRank;
          currRank++;
        }
      }
    }
    //console.log("SORTED:", tempPlayersArray);
    //re arrage array for next comp
    var newArray = new Array(tempPlayersArray.length);
    //var foo = new Array(45)
    //console.log(tempPlayersArray);
    //first 3 Players
    console.log("FIRST 3 PLAYERS");
    for(var i = 0; i < 3; i++){
      newArray.splice(i+rrSeq[i], 1, tempPlayersArray[i]);
    }
    console.log(newArray);
    console.log("MIDDLE PLAYERS");
    //middle Players
    var n = (newArray.length/2)-2;
    var position = 3;
    for(var i = 0; i < 2*n; i++) {
			if(position % 2 == 0) {
				//temp[position + movement[4]] = players[position];
        newArray.splice(position+rrSeq[4], 1, tempPlayersArray[position]);
				//temp.set(position+movement[4], players.get(position));
				position++;
			}else {
				//temp[position + movement[3]] = players[position];
        newArray.splice(position+rrSeq[3], 1, tempPlayersArray[position]);
				//temp.set(position+movement[3], players.get(position));
				position++;
			}
		}
    console.log(newArray);
    console.log("LAST PLAYER");
    //last player
    console.log(newArray.length-2);
    console.log(tempPlayersArray, tempPlayersArray[newArray.length-1]);
    newArray.splice(newArray.length-2, 1, tempPlayersArray[newArray.length-1]);
    //console.log(newArray);

    // for(var y = 0; y < tempPlayersArray.length; y++){
    //   var nextPos = (y+rrSeq[y]);
    //   var nextPlayer = tempPlayersArray[nextPos];
    //   //console.log(nextPlayer);
    //   newArray.push(nextPlayer);
    // }
    console.log(newArray);
    //console.log(tempPlayersArray);
    db.update({ _id: displayedTournament.id }, { $set: { players: newArray, currentRound: nextRoundVar} }, { multi: true }, function (errVar, numReplaced) {
      //console.log("Replaced Num: " + numReplaced);
      if(errVar){
        console.log("ERROR: " + errVar);
      }
      //console.log("ERROR: " + errVar);
      //console.log("DB Updated");
      //refresh ladder - order by rank

      // NOTE: get all players and update their pos based on scores
      //rotate round robbin
      //display next round
      displayTournament(displayedTournament);
    });
  });
}
function compareScores(p1, p2){
  //console.log("compare Scores");
  //console.log(Math.round((p1.win/p1.played + 0.00001) * 100) + " > " + Math.round((p2.win/p2.played + 0.00001) * 100));
  if(Math.round((p1.win/p1.played + 0.00001) * 100) > Math.round((p2.win/p2.played + 0.00001) * 100)){
    return 1;
  }else if(Math.round((p1.win/p1.played + 0.00001) * 100) < Math.round((p2.win/p2.played + 0.00001) * 100)){
    return -1;
  }else{
    if(Math.round((p1.setsWin/(p1.setsWin + p1.setsLoss) + 0.00001) * 100) > Math.round((p2.setsWin/(p2.setsWin + p2.setsLoss) + 0.00001) * 100)){
      return 1;
    }else if (Math.round((p1.setsWin/(p1.setsWin + p1.setsLoss) + 0.00001) * 100) < Math.round((p2.setsWin/(p2.setsWin + p2.setsLoss) + 0.00001) * 100)) {
      return -1;
    }else{
      if(Math.round((p1.gameWin/(p1.gameWin + p1.gameLoss) + 0.00001) * 100) > Math.round((p2.gameWin/(p2.gameWin + p2.gameLoss) + 0.00001) * 100)){
        return 1;
      }else if(Math.round((p1.gameWin/(p1.gameWin + p1.gameLoss) + 0.00001) * 100) < Math.round((p2.gameWin/(p2.gameWin + p2.gameLoss) + 0.00001) * 100)){
        return -1;
      }else{
        return 1;
        console.log("Player scores are equal");
      }
    }
  }
}
